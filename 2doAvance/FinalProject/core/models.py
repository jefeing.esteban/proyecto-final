from django.db import models

# Create your models here.

## MODEL CARRERAS
class Carreras(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Nombre = models.CharField(db_column='Nombre', default="TIC´s", max_length=64, null=False)
    #Grupos = models.ManyToManyField('Grupo', through='CARRERA_GRUPOS')
    #Docentes = models.ManyToManyField('Docente', through='CARRERA_DOCENTE')

    def __str__(self):
        return self.Nombre

    class Meta:
        managed = False
        db_table = 'CARRERAS'


## MODEL GRUPOS
class Grupos(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=2)
    Grado = models.IntegerField(db_column='Grado', null=False)
    Grupo = models.CharField(db_column='Grupo', max_length=1, null=False)
    #Carreras = models.ManyToManyField('Carrera', through='CARRERA_GRUPOS')

    def __str__(self):
        return self.Grupo

    class Meta:
        managed = False
        db_table = 'GRUPOS'


## MODEL CARRERA-GRUPOS
class Carrera_Grupos(models.Model):
    Carrera = models.ForeignKey(Carreras, db_column='Carrera', on_delete=models.CASCADE, null=False)
    Grupo = models.ForeignKey(Grupos, db_column='Grupo', on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.Carrera} y {self.Grupo}"

    class Meta:
        managed = False
        db_table = 'CARRERA_GRUPOS'


## MODEL ALUMNOS
class Alumnos(models.Model):
    Matricula = models.CharField(db_column='Matricula', primary_key=True, null=False, max_length=4)
    Grupo = models.ForeignKey(Grupos, db_column='Grupo', on_delete=models.CASCADE, null=False)
    Carrera = models.ForeignKey(Carreras, db_column='Carrera', on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.Grupo} y {self.Carrera}"

    class Meta:
        managed = False
        db_table = 'ALUMNOS'


## MODEL PUESTOS
class Puestos(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Nombre = models.CharField(db_column='Nombre', default="Administración", max_length=64, null=False)
    #Departamentos = models.ManyToManyField('Departamento', through='PUESTO_DEPTO')

    def __str__(self):
        return self.Nombre

    class Meta:
        managed = False
        db_table = 'PUESTOS'


## MODEL DEPARTAMENTOS
class Departamentos(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Nombre = models.CharField(db_column='Nombre', default="Contabilidad", null=False, max_length=64)
    #Puestos = models.ManyToManyField('Puesto', through='PUESTO_DEPTO')

    def __str__(self):
        return self.Nombre

    class Meta:
        managed = False
        db_table = 'DEPARTAMENTOS'


## MODEL PUESTO-DEPTO
class Puesto_Depto(models.Model):
    Puesto = models.ForeignKey(Puestos, db_column='Puesto', on_delete=models.CASCADE, null=False)
    Departamento = models.ForeignKey(Departamentos, db_column='Departamento', on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.Puesto} y {self.Departamento}"
    class Meta:
        managed = False
        db_table = 'PUESTO_DEPTO'


## MODEL ADMINISTRATIVOS
class Administrativos(models.Model):
    Numero = models.AutoField(db_column='Numero', primary_key=True)
    Puesto = models.ForeignKey(Puestos, db_column='Puesto', on_delete=models.CASCADE, null=False)
    Departamento = models.ForeignKey(Departamentos, models.CASCADE, db_column='Departamento', null=False)

    def __str__(self):
        return str(self.Numero)

    class Meta:
        managed = False
        db_table = 'ADMINISTRATIVOS'


## MODEL DOCENTES
class Docentes(models.Model):
    Numero = models.AutoField(db_column='Numero', primary_key=True)
    #Carreras = models.ManyToManyField('Carrera', through='CARRERA_DOCENTE')

    def __str__(self):
        return str(self.Numero)

    class Meta:
        managed = False
        db_table = 'DOCENTES'


## MODEL CARRERA_DOCENTE
class Carrera_Docente(models.Model):
    Docente = models.ForeignKey(Docentes, db_column='Docente', on_delete=models.CASCADE, null=False)
    Carrera = models.ForeignKey(Carreras, db_column='Carrera', on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.Docente} y {self.Carrera}"

    class Meta:
        managed = False
        db_table = 'CARRERA_DOCENTE'


## MODEL GÉNEROS
class Generos(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Genero = models.CharField(db_column='Genero', default="Masculino", max_length=32)

    def __str__(self):
        return self.Genero

    class Meta:
        managed = False
        db_table = 'GENEROS'


## MODEL ESTADOS
class Estados(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Estado = models.CharField(db_column='Estado', default="Steve", max_length=32)

    def __str__(self):
        return self.Estado

    class Meta:
        managed = False
        db_table = 'ESTADOS'


## MODEL USUARIOS
class Usuarios(models.Model):
    Numero = models.AutoField(db_column='Numero',primary_key=True)
    #HuellaDigital = models.BinaryField()
    NombrePila = models.CharField(db_column='NombrePla', default="Steve", max_length=128)
    ApPaterno = models.CharField(db_column='ApPaterno', default="Olmos", max_length=64)
    ApMaterno = models.CharField(db_column='ApMaterno', default="Labastida", max_length=64)
    Genero = models.ForeignKey(Generos, on_delete=models.CASCADE, db_column='Genero',)
    FechaNacimiento = models.DateTimeField(db_column='FechaNacimiento', auto_now_add=True, auto_now=False)
    NumTel = models.CharField(db_column='NumTel', default="666-666-96-98", max_length=16)
    Email = models.CharField(db_column='Email', default="@gmail.com", max_length=128)
    Calle = models.CharField(db_column='Calle', default="Calle X", max_length=128)
    Colonia = models.CharField(db_column='Colonia', default="Refugio", max_length=128)
    NumExterior = models.CharField(db_column='NumExterior', default="1234", max_length=4)
    Alumno = models.ForeignKey(Alumnos, on_delete=models.CASCADE, db_column='Alumno', null=False)
    Docente = models.ForeignKey(Docentes, on_delete=models.CASCADE, db_column='Docente', null=False)
    Administrativo = models.ForeignKey(Administrativos, on_delete=models.CASCADE, db_column='Administrativo',)
    EstadoActual = models.ForeignKey(Estados, on_delete=models.CASCADE, db_column='EstadoActual',)
    FechaRegistro = models.DateTimeField(db_column='FechaRegistro',auto_now_add=True, auto_now=False)

    def __str__(self):
        return f"{self.NombrePila} {self.ApPaterno} {self.ApMaterno}"

    class Meta:
        managed = False
        db_table = 'USUARIOS'


## MODEL DOCENCIAS
class Docencias(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Nombre = models.CharField(db_column='Nombre', default="Steve", max_length=64)

    def __str__(self):
        return self.Nombre

    class Meta:
        managed = False
        db_table = 'DOCENCIAS'


## MODEL UBICACIONES
class Ubicaciones(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Piso = models.CharField(db_column='Piso', default="Steve", max_length=1)
    Seccion = models.CharField(db_column='Seccion', default="Steve", max_length=1)
    Docencia = models.ForeignKey(Docencias, models.CASCADE, db_column='Docencia', null=False)

    def __str__(self):
        return f"{self.Piso} y {self.Seccion}"

    class Meta:
        managed = False
        db_table = 'UBICACIONES'


## MODEL TAMAÑOS
class Tamanos(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Descripcion = models.CharField(db_column='Descripcion', max_length=64, null=False)
    Dimensiones = models.CharField(db_column='Dimensiones', max_length=16, null=False)
    Precio = models.FloatField(db_column='Precio', blank=True, null=True)

    def __str__(self):
        return self.Descripcion

    class Meta:
        managed = False
        db_table = 'TAMANOS'


## MODEL DISPONIBILIDAD
class Disponibilidad(models.Model):
    Codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=4)
    Descripcion = models.CharField(db_column='Descripcion', max_length=64, null=False)

    def __str__(self):
        return self.Descripcion

    class Meta:
        managed = False
        db_table = 'DISPONIBILIDAD'


## MODEL CASILLEROS
class Casilleros(models.Model):
    Numero = models.AutoField(db_column='Numero', primary_key=True)
    Disponibilidad = models.ForeignKey(Disponibilidad, models.CASCADE, db_column='Disponibilidad', null=False)
    Ubicacion = models.ForeignKey(Ubicaciones, models.CASCADE, db_column='Ubicacion', null=False)
    Tamano = models.ForeignKey(Tamanos, models.CASCADE, db_column='Tamano', null=False)

    def __str__(self):
        return str(self.Numero)

    class Meta:
        managed = False
        db_table = 'CASILLEROS'


## MODEL PERIODOS
class Periodos(models.Model):
    Folio = models.CharField(db_column='Folio', primary_key=True, max_length=8)
    Meses = models.CharField(db_column='Meses', max_length=32, null=False)

    def __str__(self):
        return self.Folio

    class Meta:
        managed = False
        db_table = 'PERIODOS'


## MODEL RENTAS
class Rentas(models.Model):
    Referencia = models.CharField(db_column='Referencia', primary_key=True, max_length=16)
    FechaInicio = models.DateTimeField(db_column='FechaInicio', auto_now_add=True, auto_now=False)
    FechaFinal = models.DateTimeField(db_column='FechaFinal', auto_now_add=True, auto_now=False)
    Usuario = models.ForeignKey(Usuarios, models.CASCADE, db_column='Usuario', null=False)
    Casillero = models.ForeignKey(Casilleros, models.CASCADE, db_column='Casillero', null=False)
    Periodo = models.ForeignKey(Periodos, models.CASCADE, db_column='Periodo', null=False)
    Estado = models.ForeignKey(Estados, models.CASCADE, db_column='Estado', null=False)

    def __str__(self):
        return self.Referencia

    class Meta:
        managed = False
        db_table = 'RENTAS'


## MODEL PAGOS
class Pagos(models.Model):
    Folio = models.AutoField(db_column='Numero', primary_key=True)
    FechaPago = models.DateTimeField(db_column='FechaPago', auto_now_add=True, auto_now=False)
    Total = models.FloatField(db_column='Total', blank=True, null=True)
    Renta = models.ForeignKey(Rentas, models.CASCADE, db_column='Renta', null=False)

    def __str__(self):
        return str(self.Folio)

    class Meta:
        managed = False
        db_table = 'PAGOS'